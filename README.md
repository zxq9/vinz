# vinz

Vinz Clortho, the Keymaster

Docs live [here](https://zxq9.com/projects/vinz/docs/).

Vinz is a simple project that abstracts some of the more confusing and tedious things about using the crypto and public_key modules for generation of RSA keys, storing them on disk, and using them for signing, encryption and verification.

Vinz is packaged as a Zomp project. `zx describe otpr-vinz` will locate it for your project if you are using ZX, this repo can be referenced directly via rebar3 or erlang.mk, or as an alternative you can just vendor the module out and drop it into a project's `src/` directory as long as you maintain attribution (MIT license, so do as you please!).

## Why use this?
The public_key, crypto and related modules provide APIs that expose most of the functionality implemented by OpenSSL and related cryptographic libraries, but in exposing so much functionality they also expose a tremendous amount of complexity that the majority of people with rather straight forward encryption needs don't really care to concern themselves with.

This library is good for people who want to:

- Generate a strong RSA key pair and store it on disk
- Optionally password protect the private key
- Sign with the private key and verify with the public key
- Encrypt with the public key and decrypt with the private key
- Not concern themselves with the details, trading the runtime overhead of strong defaults for development simplicity


## Current defaults
Vinz currently defaults to overkill in every area:

- SHA-512 hashes
- 16384-bit key length
- Standard 65537 public exponent
- RC2-CBC private key password with a strong random salt
- OAEP padding for RSA encryption

These defaults may change in the future as stronger techniques come out or become supported by the API made available in Erlang's standard library. It is not anticipated that this will cause any breaking changes to the API. Many of the defaults are adjustable by calling interface functions that expose the necessary arguments.

## Example of use in the shell

```
    1> {ok, KeyPath, PubPath} = vinz:generate_rsa().
    {ok,"./PGASDJOGNQIFDAO3MV8DE0KUV7QKTTUBE0GVJ4P4UTHKBIAWTN0W6ZYNOL1TFRVEU6FKKJZXBTW0I4I3B3LL837HO06Y6NFAUWB.key.der",
        "./PGASDJOGNQIFDAO3MV8DE0KUV7QKTTUBE0GVJ4P4UTHKBIAWTN0W6ZYNOL1TFRVEU6FKKJZXBTW0I4I3B3LL837HO06Y6NFAUWB.pub.der"}
    2> TestData = <<"This is some binary test data.">>.
    <<"This is some binary test data.">>
    3> {ok, Key} = vinz:load(KeyPath). 
    {ok, {'RSAPrivateKey', ...}}
    4> Signature = vinz:sign(TestData, Key).
    <<106,57,91,215,118,0,60,186,196,1,151,247,220,147,104,44,
      3,193,210,65,123,16,87,143,148,238,37,232,62,...>>
    5> {ok, Pub} = vinz:load(PubPath).
    {ok, {'RSAPublicKey', ...}}
    6> vinz:verify(TestData, Signature, Pub).
    true
    7> CypherText = vinz:encrypt_with_public(TestData, Pub).
    <<69,196,126,78,160,255,180,75,4,28,166,49,200,31,40,115,
      113,97,164,1,77,189,158,2,77,3,135,216,111,...>>
    8> vinz:decrypt_with_private(CypherText, Key).
    <<"This is some binary test data.">>
    9> vinz:save_pem(".", "foo_key", Key, "some_weak_password").
    {ok,"./foo_key.key.pem"}
```
