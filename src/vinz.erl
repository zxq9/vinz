%%% @doc
%%% Vinz Clortho, the Keymaster
%%%
%%% A small library module to simplify cross-platform generation and use of RSA keys.
%%% @end

-module(vinz).
-vsn("2.0.0").
-author("Craig Everett <zxq9@zxq9.com>").
-copyright("Craig Everett <zxq9@zxq9.com>").
-license("MIT").

-export([generate_rsa/0, generate_rsa/1, generate_rsa/2, generate_rsa/3, generate_rsa/4,
         save_der/3,     save_pem/3,     save_pem/4,
         load/1,         load/2,         label_to_hash/1,
         sign/2,         verify/3,
         encrypt_with_public/2,          encrypt_with_private/2,
         decrypt_with_public/2,          decrypt_with_private/2]).

-include_lib("public_key/include/OTP-PUB-KEY.hrl").

-define(cryptopts, [{rsa_padding, rsa_pkcs1_oaep_padding}]).


-spec generate_rsa() -> Result
    when Result   :: {ok, KeyPath, PubPath}
                   | {error, file:posix()},
         KeyPath  :: file:filename(),
         PubPath  :: file:filename().
%% @doc
%% Generate a 16384-bit RSA key pair and store them in the current working directory
%% as DER binaries under file names derived from the SHA-512 hash of the public half.
%% @equiv vinz:generate_rsa(".")

generate_rsa() ->
    generate_rsa(".").


-spec generate_rsa(Dir) -> Result
    when Dir     :: file:filename(),
         Result   :: {ok, KeyPath, PubPath}
                   | {error, file:posix()},
         KeyPath  :: file:filename(),
         PubPath  :: file:filename().
%% @doc
%% Generate a 16384-bit RSA key pair and store them in the directory `Dir' as DER
%% binaries under file names derived from the SHA-512 hash of the public half.
%% @equiv vinz:generate_rsa(Dir, hash)

generate_rsa(Dir) ->
    generate_rsa(Dir, hash).


-spec generate_rsa(Dir, Name) -> Result
    when Dir      :: file:filename(),
         Name     :: hash | string(),
         Result   :: {ok, KeyPath, PubPath}
                   | {error, file:posix()},
         KeyPath  :: file:filename(),
         PubPath  :: file:filename().
%% @doc
%% Generate a 16384-bit RSA key pair and store them in the directory `Dir' as DER
%% binaries under file names derived from the provided string `Name'.
%% @equiv vinz:generate_rsa(Dir, Name, 16384)

generate_rsa(Dir, Name) ->
    generate_rsa(Dir, Name, 16384).


-spec generate_rsa(Dir, Name, Size) -> Result
    when Dir     :: file:filename(),
         Name    :: hash | string(),
         Size    :: pos_integer(),
         Result   :: {ok, KeyPath, PubPath}
                   | {error, file:posix()},
         KeyPath  :: file:filename(),
         PubPath  :: file:filename().
%% @doc
%% Generate an RSA key pair of length `Size' and store them in the directory `Dir' as DER
%% binaries under file names derived from the provided string `Name'.
%%
%% NOTE: Common sizes are 2048, 4096, 8192, and 16384. Keys smaller than 4096 are not
%% considered to be secure over a long period to an attacker with sufficient resources.
%% @equiv vinz:generate_rsa(Dir, Name, Size, none)

generate_rsa(Dir, Name, Size) ->
    generate_rsa(Dir, Name, Size, none).


-spec generate_rsa(Dir, Name, Size, Password) -> Result
    when Dir      :: file:filename(),
         Name     :: hash | string(),
         Size     :: pos_integer(),
         Password :: none | string(),
         Result   :: {ok, KeyPath, PubPath}
                   | {error, file:posix()},
         KeyPath  :: file:filename(),
         PubPath  :: file:filename().
%% @doc
%% Generate an RSA key pair of length `Size' and store them in the directory `Dir' as
%% PEM encoded files under file names derived from the provided string `Name' with the
%% private key protected by the password `Password'.
%%
%% NOTE 1: Common sizes are 2048, 4096, 8192, and 16384. Keys smaller than 4096 are not
%% considered to be secure over a long period to an attacker with sufficient resources.
%%
%% NOTE 2: Insufficiently strong key passwords can be vulnerable to dictionary attacks.

generate_rsa(Dir, Name, Size, Password) ->
    Key =
        #'RSAPrivateKey'{modulus        = Mod,
                         publicExponent = PE} =
            public_key:generate_key({rsa, Size, 65537}),
    Pub =
        #'RSAPublicKey'{modulus        = Mod,
                        publicExponent = PE},
    Label = label(Name, Pub),
    do_generate_rsa(Dir, Label, Key, Pub, Password).

do_generate_rsa(Dir, Label, Key, Pub, none) ->
    case save_der(Dir, Label, Key) of
        {ok, KeyPath} -> save_rsa(fun save_der/3, Dir, Label, Pub, KeyPath);
        Error         -> Error
    end;
do_generate_rsa(Dir, Label, Key, Pub, Password) ->
    case save_pem(Dir, Label, Key, Password) of
        {ok, KeyPath} -> save_rsa(fun save_pem/3, Dir, Label, Pub, KeyPath);
        Error         -> Error
    end.

save_rsa(Fun, Dir, Label, Pub, KeyPath) ->
    case Fun(Dir, Label, Pub) of
        {ok, PubPath} ->
            {ok, KeyPath, PubPath};
        Error ->
            ok = file:delete(KeyPath),
            Error
    end.


label(hash, Pub) ->
    KeyHash = crypto:hash(sha512, public_key:der_encode('RSAPublicKey', Pub)),
    Size = byte_size(KeyHash) * 8,
    <<N:Size>> = KeyHash,
    integer_to_list(N, 36);
label(Name, _) ->
    Name.


label_to_hash(KeyLabel) ->
    N = list_to_integer(KeyLabel, 36),
    <<N:512>>.


-spec save_der(Dir, Name, Key) -> Result
    when Dir    :: file:filename(),
         Name   :: string(),
         Key    :: public_key:rsa_public_key() | private_key:rsa_private_key(),
         Result :: {ok, Path :: file:filename()}
                 | {error, file:posix()}.
%% @doc
%% Encode a key to DER and store it as a file with a path derived from `Dir', `Name'
%% and the key type provided.

save_der(Dir, Name, Key) ->
    Type = element(1, Key),
    DER = public_key:der_encode(Type, Key),
    Path = filename:join(Dir, name_der(Type, Name)),
    safe_write(Path, DER).


-spec save_pem(Dir, Name, Key) -> Result
    when Dir    :: file:filename(),
         Name   :: string(),
         Key    :: public_key:rsa_public_key() | private_key:rsa_private_key(),
         Result :: {ok, Path :: file:filename()}
                 | {error, file:posix()}.
%% @doc
%% Encode a key to PEM and store it as a file with a path derived from `Dir', `Name'
%% and the key type provided.

save_pem(Dir, Name, Key) ->
    Type = element(1, Key),
    Entry = public_key:pem_entry_encode(Type, Key),
    PEM = public_key:pem_encode([Entry]),
    Path = filename:join(Dir, name_pem(Type, Name)),
    safe_write(Path, PEM).


-spec save_pem(Dir, Name, Key, Password) -> Result
    when Dir      :: file:filename(),
         Name     :: string(),
         Key      :: public_key:rsa_public_key() | private_key:rsa_private_key(),
         Password :: string(),
         Result   :: {ok, Path :: file:filename()}
                   | {error, file:posix()}.
%% @doc
%% Encode a key to PEM and store it as a file with a path derived from `Dir', `Name'
%% and the key type provided, encrypted on disk using `Password' and a strong random
%% salt.
%%
%% NOTE: It is usually not favorable to password protect the public half of a key pair,
%% though this is entirely possible to do.

save_pem(Dir, Name, Key, Password) ->
    Type = element(1, Key),
    CipherInfo = {{"RC2-CBC", crypto:strong_rand_bytes(8)}, Password},
    Entry = public_key:pem_entry_encode(Type, Key, CipherInfo),
    PEM = public_key:pem_encode([Entry]),
    Path = filename:join(Dir, name_pem(Type, Name)),
    safe_write(Path, PEM).


safe_write(Path, Bin) ->
    case file:write_file(Path, Bin, [exclusive]) of
        ok    -> {ok, Path};
        Error -> Error
    end.


-spec load(Path) -> Result
    when Path       :: file:filename(),
         Result     :: {ok, DecodedKey}
                     | {error, Reason}
                     | error,
         Reason     :: bad_extension | file:posix(),
         DecodedKey :: public_key:rsa_public_key() | public_key:rsa_private_key().
%% @doc
%% Read and decode an unencrypted key from disk (public or private) located at `Path'.
%% Returns `error' if the file is found but the decoding procedure fails or if
%% a private key is password protected.
%%
%% NOTE: This function requires that the key naming extension scheme used by this library
%% is followed by the file locations provided in `Path' ("*.key.der", "*.pub.der",
%% "*.key.pem", and "*.pub.pem").
%% @equiv load(Path, none)

load(Path) ->
    load(Path, none).


-spec load(Path, Password) -> Result
    when Path       :: file:filename(),
         Password   :: none | string(),
         Result     :: {ok, DecodedKey}
                     | {error, file:posix()}
                     | error,
         DecodedKey :: public_key:rsa_public_key() | public_key:rsa_private_key().
%% @doc
%% Same as `load/1' above, but accepts an optional `Password' argument (or 'none')
%% in order to load password protected keys.
%% Returns `error' if the file is found but the decoding procedure fails or if the
%% password provided is bad.

load(Path, Password) ->
    case string:lexemes(filename:basename(Path), [$.]) of
        [_, "key", "der"] -> load_der(private, Path);
        [_, "pub", "der"] -> load_der(public, Path);
        [_, "key", "pem"] -> load_pem(Path, Password);
        [_, "pub", "pem"] -> load_pem(Path, Password);
        _                 -> {error, bad_extension}
    end.

load_der(Type, Path) ->
    case file:read_file(Path) of
        {ok, DER} -> {ok, public_key:der_decode(der_label(Type), DER)};
        Error     -> Error
    end.


load_pem(Path, Password) ->
    case file:read_file(Path) of
        {ok, Bin} -> load_pem2(Bin, Password);
        Error     -> Error
    end.

load_pem2(Bin, Password) ->
    case public_key:pem_decode(Bin) of
        [Entry] -> load_pem3(Entry, Password);
        []      -> {error, bad_format}
    end.

load_pem3(Entry, none) ->
    try
        {ok, public_key:pem_entry_decode(Entry)}
    catch
        _:_ -> error
    end;
load_pem3(Entry, Password) ->
    try
        {ok, public_key:pem_entry_decode(Entry, Password)}
    catch
        _:_ -> error
    end.


-spec sign(Data, Key) -> Signature
    when Data      :: binary(),
         Key       :: public_key:rsa_private_key(),
         Signature :: binary().
%% @doc
%% Sign binary `Data' with an RSA private key using SHA512 as the hash algorithm.
%% The signature itself will be a 64-byte binary.

sign(Data, Key) ->
    public_key:sign(Data, sha512, Key).


-spec verify(Data, Signature, PubKey) -> boolean()
    when Data      :: binary(),
         Signature :: binary(),
         PubKey    :: public_key:rsa_public_key().
%% @doc
%% Verify an RSA `Signature' of `Data' with the public key half.

verify(Data, Signature, PubKey) ->
    public_key:verify(Data, sha512, Signature, PubKey).


-spec encrypt_with_public(PlainText, Key) -> CypherText
    when PlainText  :: binary(),
         Key        :: public_key:rsa_public_key(),
         CypherText :: binary().
%% @doc
%% Encrypt binary data with a public key.

encrypt_with_public(PlainText, Key) ->
    public_key:encrypt_public(PlainText, Key, ?cryptopts).


-spec encrypt_with_private(PlainText, Key) -> CypherText
    when PlainText  :: binary(),
         Key        :: public_key:rsa_public_key(),
         CypherText :: binary().
%% @doc
%% Encrypt binary data with a public key.

encrypt_with_private(PlainText, Key) ->
    public_key:encrypt_public(PlainText, Key, ?cryptopts).


-spec decrypt_with_public(CypherText, Key) -> Result
    when CypherText :: binary(),
         Key        :: public_key:rsa_private_key(),
         Result     :: {ok, PlainText}
                     | {error, Reason :: term()},
         PlainText  :: binary().
%% @doc
%% Decrypt binary data with a private key.

decrypt_with_public(CypherText, Key) ->
    try
        {ok, public_key:decrypt_public(CypherText, Key, ?cryptopts)}
    catch
        error:Reason -> {error, Reason}
    end.


-spec decrypt_with_private(CypherText, Key) -> Result
    when CypherText :: binary(),
         Key        :: public_key:rsa_private_key(),
         Result     :: {ok, PlainText}
                     | {error, Reason :: term()},
         PlainText  :: binary().
%% @doc
%% Decrypt binary data with a private key.

decrypt_with_private(CypherText, Key) ->
    try
        {ok, public_key:decrypt_private(CypherText, Key, ?cryptopts)}
    catch
        error:Reason -> {error, Reason}
    end.


der_label(private) -> 'RSAPrivateKey';
der_label(public)  -> 'RSAPublicKey'.


name_der(public,          Name) -> Name ++ ".pub.der";
name_der(private,         Name) -> Name ++ ".key.der";
name_der('RSAPublicKey',  Name) -> Name ++ ".pub.der";
name_der('RSAPrivateKey', Name) -> Name ++ ".key.der".


name_pem(public,          Name) -> Name ++ ".pub.pem";
name_pem(private,         Name) -> Name ++ ".key.pem";
name_pem('RSAPublicKey',  Name) -> Name ++ ".pub.pem";
name_pem('RSAPrivateKey', Name) -> Name ++ ".key.pem".
